/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __TEST_LOG_ENTRY_PROPERTY__6AD308385AC8E84EA05CC9146AD22289
#define __TEST_LOG_ENTRY_PROPERTY__6AD308385AC8E84EA05CC9146AD22289

#include "libgrill/logger/create_log_entry_request.hh"

namespace LibGrill {
namespace Test {

void print(const LibGrill::Logger::Properties&);
void test_log_entry_property();

} // namespace LibGrill::Test
} // namespace LibGrill

#endif
