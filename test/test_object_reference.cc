/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_object_reference.hh"

#include <iostream>
#include <string>

namespace LibGrill {
namespace Logger {

template <>
ObjectReference to_object_reference(const int& _value)
{
    return ObjectReference{ObjectReferenceType{"int"}, {ObjectReferenceValue{std::to_string(_value)}}};
}

template <>
ObjectReference to_object_reference(const std::string& _value)
{
    return ObjectReference{ObjectReferenceType{"string"}, {ObjectReferenceValue{_value}}};
}

} // namespace LibGrill::Logger

namespace Test {

void print(const LibGrill::Logger::ObjectReferences& object_references)
{
    for (const auto item : object_references)
    {
        std::cout << item.first << ':';
        for (const auto value : item.second)
        {
            std::cout << ' ' << value;
        }
        std::cout << std::endl;
    }
}

void test_object_reference()
{
    Logger::ObjectReferences object_references;

    Logger::add(object_references, 7);
    add(object_references, 666);
    add(object_references, 42);
    add(object_references, std::string("text"));
    print(object_references);
}

} // namespace LibGrill::Test
} // namespace LibGrill

