# Copyright (C) 2021  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.5)

find_package(Boost 1.53.0
    COMPONENTS
        system
        program_options
        unit_test_framework
    REQUIRED)

add_executable(test-libgrill
    test_log_entry_property.cc
    test_object_reference.cc
    test.cc)

set_target_properties(test-libgrill PROPERTIES
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/bin")

target_include_directories(test-libgrill
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(test-libgrill
    PRIVATE
        LibGrill::library
        Boost::system
        Boost::program_options
        Boost::unit_test_framework)


