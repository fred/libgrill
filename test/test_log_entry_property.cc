/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "test_log_entry_property.hh"

#include <iostream>
#include <string>

namespace LibGrill {
namespace Logger {

template <>
LogEntryProperty to_log_entry_property(const int& _value)
{
    return LogEntryProperty{LogEntryPropertyType{"number"}, LogEntryPropertyValue{std::to_string(_value)}};
}

template <>
LogEntryProperty to_log_entry_property(const std::string& _value)
{
    return LogEntryProperty{LogEntryPropertyType{"string"}, LogEntryPropertyValue{_value}};
}

} // namespace LibGrill::Logger

namespace Test
{

void print(const LibGrill::Logger::Properties& properties)
{
    for (const auto item : properties)
    {
        std::cout << item.first << ":\n";
        for (const auto value : item.second)
        {
            std::cout << "  " << value.value;
            if (value.children.empty())
            {
                std::cout << " (no children)\n";
            }
            else
            {
                std::cout << " (children:";
                for (const auto& child : value.children)
                {
                    std::cout << " " << child.first << ":" << child.second;
                }
                std::cout << ")\n";
            }
        }
        std::cout << std::endl;
    }
}

void test_log_entry_property()
{
    LibGrill::Logger::Properties properties;
    LibGrill::Logger::add(properties, 11);
    LibGrill::Logger::add(properties, 666);
    auto& log_entry_property = LibGrill::Logger::add(properties, 13);
    Logger::add(x, std::string("nice"));
    Logger::add(x, 8);

    LibGrill::Logger::add(properties, std::string("good"));
    LibGrill::Logger::add(properties, std::string("cool"));
    print(properties);
}


} // namespace LibGrill::Test
} // namespace LibGrill
