/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __LOG_ENTRY_REQUEST__DEB0975320EE89C8A03C29475999727A
#define __LOG_ENTRY_REQUEST__DEB0975320EE89C8A03C29475999727A

#include "libgrill/logger/log_entry_property.hh"
#include "libgrill/logger/log_entry_strong_types.hh"
#include "libgrill/logger/object_reference.hh"

#include <boost/optional.hpp>

namespace LibGrill {
namespace Logger {

class LogEntryRequest
{
public:
    LogEntryRequest() = default;
    void add(const SessionId&);
    void add(const LogEntryContent&);
    void add(Properties);
    void add(ObjectReferences);
    template<typename Type>
    void add_object_reference(const Type&);
    template<typename Type>
    void add_property(const Type&);

    bool has_session_id() const;
    bool has_content() const;
    bool has_properties() const;
    bool has_references() const;

    SessionId get_session_id() const;
    LogEntryContent get_content() const;
    const Properties& get_properties() const;
    const ObjectReferences& get_references() const;

protected:
    Properties properties_;
    ObjectReferences references_;
    boost::optional<SessionId> session_id_;
    boost::optional<LogEntryContent> content_;
};

template<typename Type>
void LogEntryRequest::add_object_reference(const Type& _value)
{
    Logger::add(references_, _value);
}

template<typename Type>
void LogEntryRequest::add_property(const Type& _value)
{
    Logger::add(properties_, _value);
}

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
