/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_ENTRY_PROPERTY_HH_E0825DB430A575EC9DD6831DF43455AE
#define LOG_ENTRY_PROPERTY_HH_E0825DB430A575EC9DD6831DF43455AE

#include "libgrill/strong_type.hh"

#include <map>
#include <vector>

namespace LibGrill {
namespace Logger {


struct LogEntryProperty
{
    using Name = StrongString<struct NameTag_>;
    using Value = StrongString<struct ValueTag_>;
    using Children = std::map<Name, Value>;
    Name name;
    Value value;
};

struct LogEntryPropertyValue
{
    LogEntryProperty::Value value;
    LogEntryProperty::Children children;
};

using LogEntryPropertyValues = std::vector<LogEntryPropertyValue>;
using Properties = std::map<LogEntryProperty::Name, LogEntryPropertyValues>;

LogEntryPropertyValue& add(Properties&, const LogEntryProperty&);
LogEntryPropertyValue& add(Properties&, const LogEntryProperty::Name&, const LogEntryPropertyValue&);

template <typename Type>
LogEntryProperty to_log_entry_property(const Type&);

template <typename ChildType>
void add(
        LogEntryPropertyValue& _log_entry_property_value,
        const ChildType& _value)
{
    const auto log_entry_property = to_log_entry_property(_value);
    _log_entry_property_value.children[log_entry_property.name] = log_entry_property.value;
}

template <typename Type>
std::enable_if_t<!std::is_same<Type, LogEntryProperty>::value, LogEntryPropertyValue&> add(
        Properties& _properties,
        const Type& _value)
{
    return add(_properties, to_log_entry_property(_value));
}

void merge(Properties&, Properties&&);

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif//LOG_ENTRY_PROPERTY_HH_E0825DB430A575EC9DD6831DF43455AE
