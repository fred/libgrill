/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CLOSE_LOG_ENTRY__6F0E1411F49B48FBFE2144A3CD5B4A9A
#define __CLOSE_LOG_ENTRY__6F0E1411F49B48FBFE2144A3CD5B4A9A

#include "libgrill/connection.hh"
#include "libgrill/exception.hh"
#include "libgrill/service.hh"
#include "libgrill/logger/close_log_entry_request.hh"

namespace LibGrill {
namespace Logger {

class CloseLogEntryException : public GrpcException
{
    using GrpcException::GrpcException;
};

void close_log_entry(Connection<Service::Logger>&, const CloseLogEntryRequest&);

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
