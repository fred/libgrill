/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CREATE_LOG_ENTRY__CB431AB5D30871F613371CCFDE205995
#define __CREATE_LOG_ENTRY__CB431AB5D30871F613371CCFDE205995

#include "libgrill/connection.hh"
#include "libgrill/exception.hh"
#include "libgrill/service.hh"
#include "libgrill/logger/create_log_entry_request.hh"
#include "libgrill/logger/log_entry_id.hh"

namespace LibGrill {
namespace Logger {

class CreateLogEntryException : public GrpcException
{
    using GrpcException::GrpcException;
};

LogEntryId create_log_entry(Connection<Service::Logger>&, const CreateLogEntryRequest&);

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
