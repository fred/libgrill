/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __LOG_ENTRY_STRONG_TYPES__B16EA7FF7967AE2573EF2A4A1AD5D9BC
#define __LOG_ENTRY_STRONG_TYPES__B16EA7FF7967AE2573EF2A4A1AD5D9BC

#include "libgrill/strong_type.hh"

namespace LibGrill {
namespace Logger {

using LogEntryContent = StrongString<struct LogEntryContentTag_>;
using LogEntryService = StrongString<struct LogEntryServiceTag_>;
using LogEntryType = StrongString<struct LogEntryTypeTag_>;
using SessionId = StrongString<struct SessionIdTag_>;
using SourceIp = StrongIp<struct SourceIpTag_>;

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
