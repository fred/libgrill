/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __LOG_ENTRY_ID__E0F4BAF332CE080D186FFF115E793DA5
#define __LOG_ENTRY_ID__E0F4BAF332CE080D186FFF115E793DA5

#include "libgrill/logger/log_entry_strong_types.hh"

#include <string>
#include <vector>

namespace LibGrill {
namespace Logger {


class LogEntryId
{
public:
    using NumericalId = StrongUnsigned<struct NumericalIdTag_>;
    using StringId = StrongString<struct StringIdTag_>;
    using TimeBegin = StrongString<struct LogEntryTimeBeginTag_>;

    explicit LogEntryId(const LogEntryId::StringId&);

    operator NumericalId() const;
    operator StringId() const;

    NumericalId get_numerical_id() const;
    StringId get_string_id() const;
    LogEntryService get_service() const;
    TimeBegin get_time_begin() const;
    bool is_from_monitored_ip_address() const;

private:
    LogEntryId(const LogEntryId::StringId& _value, const std::vector<std::string>& _tokens);

private:
    const StringId unparsed_value_;
    const NumericalId numerical_id_;
    const TimeBegin time_begin_;
    const LogEntryService service_;
    const bool monitoring_status_;
};


} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
