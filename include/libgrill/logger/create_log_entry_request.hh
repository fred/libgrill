/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CREATE_LOG_ENTRY_REQUEST__AC3C92268CED9C89BB8E11174959457D
#define __CREATE_LOG_ENTRY_REQUEST__AC3C92268CED9C89BB8E11174959457D

#include "libgrill/logger/log_entry_request.hh"

#include <boost/optional.hpp>

namespace LibGrill {
namespace Logger {

class CreateLogEntryRequest : public LogEntryRequest
{
public:
    CreateLogEntryRequest(const LogEntryType&, const LogEntryService&);

    using LogEntryRequest::add;
    void add(const SourceIp&);
    bool has_source_ip() const;

    LogEntryService get_service() const;
    LogEntryType get_type() const;
    SourceIp get_source_ip() const;

private:
    LogEntryType type_;
    LogEntryService service_;
    boost::optional<SourceIp> source_ip_;
};
} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
