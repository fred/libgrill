/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CLOSE_LOG_ENTRY_REQUEST__90738EC9E68F6E2BA209C712BBD8B7EF
#define __CLOSE_LOG_ENTRY_REQUEST__90738EC9E68F6E2BA209C712BBD8B7EF

#include "libgrill/logger/log_entry_id.hh"
#include "libgrill/logger/log_entry_request.hh"

namespace LibGrill {
namespace Logger {

using OperationResultCode = StrongUnsigned<struct OperationResultCodeTag_>;
using OperationResultName = StrongString<struct OperationResultNameTag_>;

class OperationResult
{
public:
    explicit OperationResult(OperationResultCode);
    explicit OperationResult(OperationResultName);
    OperationResult(const OperationResult&);
    ~OperationResult();

    bool has_code() const;
    bool has_name() const;
    const OperationResultCode& get_code() const;
    const OperationResultName& get_name() const;
private:
    enum class Content
    {
        code,
        name
    };
    Content content_;
    union
    {
        OperationResultCode code_;
        OperationResultName name_;
    };
};

class CloseLogEntryRequest : public LogEntryRequest
{
public:
    CloseLogEntryRequest(const LogEntryId&, const OperationResultCode&);
    CloseLogEntryRequest(const LogEntryId&, const OperationResultName&);

    using LogEntryRequest::add;
    OperationResult get_operation_result() const;
    LogEntryId get_log_entry_id() const;

private:
    LogEntryId log_entry_id_;
    OperationResult operation_result_;
 };

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
