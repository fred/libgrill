/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __OBJECT_REFERENCE__BB0E7F8EBA4BCE0CF59650FF72CC22D6
#define __OBJECT_REFERENCE__BB0E7F8EBA4BCE0CF59650FF72CC22D6

#include "libgrill/strong_type.hh"

#include <map>
#include <vector>

namespace LibGrill {
namespace Logger {

using ObjectReferenceType = StrongString<struct ObjectReferenceTypeTag_>;
using ObjectReferenceValue = StrongString<struct ObjectReferenceValueTag_>;
using ObjectReferenceValues = std::vector<ObjectReferenceValue>;
using ObjectReferences = std::map<ObjectReferenceType, ObjectReferenceValues>;

struct ObjectReference
{
    ObjectReferenceType type;
    ObjectReferenceValues values;
};

void add(ObjectReferences&, const ObjectReference&);

template <typename Type>
ObjectReference to_object_reference(const Type&);

template <typename Type, typename = std::enable_if_t<not std::is_same<Type, ObjectReference>{}>>
void add(ObjectReferences& _dst, const Type& _value)
{
    add(_dst, to_object_reference<Type>(_value));
}

void merge(ObjectReferences&, ObjectReferences&&);

} // namespace LibGrill::Logger
} // namespace LibGrill

#endif
