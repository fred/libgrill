/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __STRONG_TYPE__DC138082DBBD40968A523B5B2CB65081
#define __STRONG_TYPE__DC138082DBBD40968A523B5B2CB65081

#include "libstrong/type.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/uuid/uuid.hpp>

#include <string>

namespace LibGrill {

namespace StrongTypeSkill = LibStrong::Skill;

template <typename Type, typename Tag, template <typename> class ...Skills>
using StrongType = LibStrong::TypeWithSkills<Type, Tag, Skills...>;

template <typename StrongType>
using Optional = LibStrong::Optional<StrongType>;

template <typename Tag>
using StrongString = StrongType<std::string, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

template <typename Tag>
using StrongUuid = StrongType<boost::uuids::uuid, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

template <typename Tag>
using StrongIp = StrongType<boost::asio::ip::address, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

template <typename Tag>
using StrongUnsigned = StrongType<uint64_t, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

} // namespace LibGrill

#endif
