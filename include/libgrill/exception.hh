/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __EXCEPTION__2DF5506F7573626F4EB6E1F60507A836
#define __EXCEPTION__2DF5506F7573626F4EB6E1F60507A836

#include <exception>
#include <string>

namespace LibGrill {

class Exception : public std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    const std::string msg_;
};

class GrpcException : Exception
{
public:
    GrpcException(int _grpc_status_error_code, std::string _grpc_status_error_message);
    int error_code() const;
    std::string error_message() const;

private:
    const int grpc_status_error_code_;
    const std::string grpc_status_error_message_;
};

} // namespace LibGrill

#endif
