/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __THROW_IF_NECESSARY__B86283BD9A88E79AADD740195CE6B88E
#define __THROW_IF_NECESSARY__B86283BD9A88E79AADD740195CE6B88E

namespace LibGrill {

template <typename ExceptionsEnum>
void throw_if_necessary(ExceptionsEnum);

} // namespace LibGrill

#endif
