/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __PROTO_WRAP__A0F5E94C4493492687B5F399035640F9
#define __PROTO_WRAP__A0F5E94C4493492687B5F399035640F9

#include "include/libgrill/strong_type.hh"

#include <google/protobuf/map.h>

#include <map>
#include <sstream>

namespace LibGrill {

template <typename>
struct InternalTypeTraits
{
    struct ProtoType;
};

template <typename InternalType>
void proto_wrap(const InternalType&, typename InternalTypeTraits<InternalType>::ProtoType*);

template <typename StrongStringTag_,
         typename = std::enable_if_t<std::is_same<
            typename InternalTypeTraits<StrongString<StrongStringTag_>>::ProtoType,
            std::string>{}>>
void proto_wrap(const StrongString<StrongStringTag_>& _src, std::string* _dst)
{
    *_dst = *_src;
}

template <typename StrongStringTag_,
         typename = std::enable_if_t<not std::is_same<
            typename InternalTypeTraits<StrongString<StrongStringTag_>>::ProtoType,
            std::string>{}>>
void proto_wrap(
        const StrongString<StrongStringTag_>& _src,
        typename InternalTypeTraits<StrongString<StrongStringTag_>>::ProtoType* _dst)
{
    _dst->set_value(*_src);
}

template <typename StrongIpTag_>
void proto_wrap(
        const StrongIp<StrongIpTag_>& _src,
        typename InternalTypeTraits<StrongIp<StrongIpTag_>>::ProtoType* _dst)
{
    std::stringstream ss;
    ss << *_src;
    _dst->set_value(ss.str());
}

template <typename InternalType>
typename InternalTypeTraits<InternalType>::ProtoType
proto_wrap(const InternalType& _src)
{
    typename InternalTypeTraits<InternalType>::ProtoType dst;
    proto_wrap(_src, &dst);
    return dst;
}

template <typename Key, typename Value,
       typename = std::enable_if_t<std::is_same<typename InternalTypeTraits<std::map<Key, Value>>::ProtoType,
           google::protobuf::Map<typename InternalTypeTraits<Key>::ProtoType, typename InternalTypeTraits<Value>::ProtoType>>{}>>
void proto_wrap(const std::map<Key, Value>& _src, typename InternalTypeTraits<std::map<Key, Value>>::ProtoType* _dst)
{
    using dst_type = typename InternalTypeTraits<std::map<Key, Value>>::ProtoType::value_type;
    for (const auto& item : _src)
    {
        _dst->insert(dst_type(*item.first, proto_wrap(item.second)));
    }
}

} // namespace LibGrill

#endif
