/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __CONNECTION_RPC_STUB_DELETER__51846F1F256F4A3E946CBF087793A76F
#define __CONNECTION_RPC_STUB_DELETER__51846F1F256F4A3E946CBF087793A76F

#include "include/libgrill/connection.hh"

#include "src/grpc_type_traits.hh"

namespace LibGrill {

template <typename S>
void Connection<S>::RpcStubDeleter::operator()(RpcStubAlias* rpc_stub_alias) const
{
    using Deleter = typename std::unique_ptr<GrpcStubType<S>>::deleter_type;
    Deleter{}(reinterpret_cast<GrpcStubType<S>*>(rpc_stub_alias));
}

} // namespace LibGrill

#endif
