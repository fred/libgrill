/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __GRPC_TYPE_TRAITS__061099158820475596C8C607AA866562
#define __GRPC_TYPE_TRAITS__061099158820475596C8C607AA866562

#include "include/libgrill/service.hh"

#include "fred_api/logger/service_logger_grpc.grpc.pb.h"

namespace LibGrill {

template <typename>
struct GrpcService;

template <typename Service>
using GrpcServiceType = typename GrpcService<Service>::Type;

template <typename Service>
using GrpcStubType = typename GrpcServiceType<Service>::Stub;

template <>
struct GrpcService<Service::Logger>
{
    using Type = Fred::Logger::Api::Logger;
};

} // namespace LibGrill

#endif
