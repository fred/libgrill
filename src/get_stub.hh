/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __GET_STUB__7A3B48EE73394674AA9EC36DDDD676C1
#define __GET_STUB__7A3B48EE73394674AA9EC36DDDD676C1

#include "include/libgrill/connection.hh"

#include "src/connection_accessor.hh"

#include <memory>

namespace LibGrill {

template <typename S>
auto& get_stub(const Connection<S>& _connection)
{
    return Connection<S>::Accessor::get_stub(_connection);
}

} // namespace LibGrill

#endif
