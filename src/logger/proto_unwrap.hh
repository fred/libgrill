/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __PROTO_UNWRAP__A8B01F449C30B45DF2BB42E474843C57
#define __PROTO_UNWRAP__A8B01F449C30B45DF2BB42E474843C57

#include "fred_api/logger/service_logger_grpc.pb.h"
#include "include/libgrill/logger/log_entry_id.hh"
#include "src/logger/close_log_entry_exception.hh"
#include "src/proto_unwrap.hh"

namespace LibGrill {

template <>
struct ProtoTypeTraits<Fred::Logger::Api::CreateLogEntryReply>
{
    using InternalType = Logger::LogEntryId;
};

template <>
struct ProtoTypeTraits<Fred::Logger::Api::CloseLogEntryReply>
{
    using InternalType = Logger::CloseLogEntryReplyException;
};

} // namespace LibGrill

#endif
