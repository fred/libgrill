/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/close_log_entry_reply.hh"
#include "src/logger/close_log_entry_exception.hh"

namespace LibGrill {

template <>
void throw_if_necessary(Logger::CloseLogEntryReplyException _exception_type)
{
    switch (_exception_type)
    {
        case Logger::CloseLogEntryReplyException::log_entry_does_not_exist:
            throw Logger::LogEntryDoesNotExist();
            break;
        case Logger::CloseLogEntryReplyException::none:
            break;
    }
}

} // namespace LibGrill
