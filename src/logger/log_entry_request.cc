/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/log_entry_request.hh"

namespace LibGrill {
namespace Logger {

void LogEntryRequest::add(const SessionId& _session_id)
{
    session_id_ = _session_id;
}

void LogEntryRequest::add(const LogEntryContent& _log_entry_content)
{
    content_ = _log_entry_content;
}

void LogEntryRequest::add(Properties _properties)
{
    merge(properties_, std::move(_properties));
}

void LogEntryRequest::add(ObjectReferences _object_references)
{
    merge(references_, std::move(_object_references));
}

bool LogEntryRequest::has_session_id() const
{
	return session_id_ != boost::none;
}

bool LogEntryRequest::has_content() const
{
	return content_ != boost::none;
}

bool LogEntryRequest::has_properties() const
{
    return !properties_.empty();
}

bool LogEntryRequest::has_references() const
{
    return !references_.empty();
}

SessionId LogEntryRequest::get_session_id() const
{
    return session_id_.value();
}

LogEntryContent LogEntryRequest::get_content() const
{
    return content_.value();
}

const Properties& LogEntryRequest::get_properties() const
{
    return properties_;
}

const ObjectReferences& LogEntryRequest::get_references() const
{
    return references_;
}

} // namespace LibGrill::Logger
} // namespace LibGrill
