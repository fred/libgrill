/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/logger/proto_unwrap.hh"

namespace LibGrill {

using namespace LibGrill::Logger;

template <>
Logger::LogEntryId proto_unwrap(const Fred::Logger::Api::CreateLogEntryReply& _src)
{
    return Logger::LogEntryId{LogEntryId::StringId{_src.data().log_entry_id().value()}};
}

template <>
Logger::CloseLogEntryReplyException proto_unwrap(const Fred::Logger::Api::CloseLogEntryReply& _src)
{
    if (_src.has_exception())
    {
        switch (_src.exception().Reasons_case())
        {
            case Fred::Logger::Api::CloseLogEntryReply_Exception::ReasonsCase::kLogEntryDoesNotExist:
                return Logger::CloseLogEntryReplyException::log_entry_does_not_exist;
            case Fred::Logger::Api::CloseLogEntryReply_Exception::ReasonsCase::REASONS_NOT_SET:
                return Logger::CloseLogEntryReplyException::none;
        }
    }
    return Logger::CloseLogEntryReplyException::none;
}

} // namespace LibGrill
