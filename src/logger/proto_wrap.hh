/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __PROTO_WRAP__0D6BB5717DFAAB8AE9E474D91634900E
#define __PROTO_WRAP__0D6BB5717DFAAB8AE9E474D91634900E

#include "fred_api/logger/service_logger_grpc.pb.h"
#include "include/libgrill/logger/close_log_entry_request.hh"
#include "include/libgrill/logger/create_log_entry_request.hh"
#include "src/proto_wrap.hh"

namespace LibGrill {

template <>
struct InternalTypeTraits<Logger::CreateLogEntryRequest>
{
    using ProtoType = Fred::Logger::Api::CreateLogEntryRequest;
};

template <>
struct InternalTypeTraits<Logger::CloseLogEntryRequest>
{
    using ProtoType = Fred::Logger::Api::CloseLogEntryRequest;
};

} // namespace LibGrill

#endif
