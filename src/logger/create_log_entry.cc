/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/create_log_entry.hh"

#include "src/get_stub.hh"

#include "src/logger/proto_wrap.hh"
#include "src/logger/proto_unwrap.hh"
#include "fred_api/logger/service_logger_grpc.pb.h"

namespace LibGrill {
namespace Logger {

LogEntryId create_log_entry(Connection<Service::Logger>& _connection, const CreateLogEntryRequest& _create_log_entry_request)
{
    grpc::ClientContext context;
    Fred::Logger::Api::CreateLogEntryRequest api_create_log_entry_request;
    Fred::Logger::Api::CreateLogEntryReply api_create_log_entry_reply;

    proto_wrap(_create_log_entry_request, &api_create_log_entry_request);
    auto stub = get_stub(_connection);
    const auto status = stub.create_log_entry(&context, api_create_log_entry_request, &api_create_log_entry_reply);

    if (not status.ok())
    {
        throw CreateLogEntryException(status.error_code(), status.error_message());
    }

    return proto_unwrap(api_create_log_entry_reply);
}

} // namespace LibGrill::Logger
} // namespace LibGrill
