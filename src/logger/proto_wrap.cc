/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/logger/proto_wrap.hh"

#include <google/protobuf/map.h>

#include <string>

namespace LibGrill {

using namespace ::LibGrill::Logger;

template <>
struct InternalTypeTraits<LogEntryService>
{
    using ProtoType = Fred::Logger::Api::LogEntryService;
};

template <>
struct InternalTypeTraits<LogEntryType>
{
    using ProtoType = Fred::Logger::Api::LogEntryType;
};

template <>
struct InternalTypeTraits<LogEntryContent>
{
    using ProtoType = Fred::Logger::Api::LogEntryContent;
};

template <>
struct InternalTypeTraits<LogEntryId>
{
    using ProtoType = Fred::Logger::Api::LogEntryId;
};

template <>
struct InternalTypeTraits<SessionId>
{
    using ProtoType = Fred::Logger::Api::SessionId;
};

template <>
struct InternalTypeTraits<SourceIp>
{
    using ProtoType = Fred::Logger::Api::SourceIp;
};

template <>
struct InternalTypeTraits<LogEntryPropertyValues>
{
    using ProtoType = Fred::Logger::Api::LogEntryPropertyValues;
};

template <>
struct InternalTypeTraits<LogEntryPropertyValue>
{
    using ProtoType = Fred::Logger::Api::LogEntryPropertyValue;
};

template <>
struct InternalTypeTraits<LogEntryProperty::Children>
{
    using ProtoType = google::protobuf::Map<std::string, std::string>;
};

template <>
struct InternalTypeTraits<LogEntryProperty::Name>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<LogEntryProperty::Value>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<ObjectReferences>
{
    using ProtoType = google::protobuf::Map<std::string, Fred::Logger::Api::ObjectReferenceValues>;
};

template <>
struct InternalTypeTraits<ObjectReferenceType>
{
    using ProtoType = std::string;
};

template <>
struct InternalTypeTraits<ObjectReferenceValue>
{
    using ProtoType = Fred::Logger::Api::ObjectReferenceValue;
};

template <>
struct InternalTypeTraits<ObjectReferenceValues>
{
    using ProtoType = Fred::Logger::Api::ObjectReferenceValues;
};

template <>
struct InternalTypeTraits<Properties>
{
    using ProtoType = google::protobuf::Map<std::string, Fred::Logger::Api::LogEntryPropertyValues>;
};

template <>
void proto_wrap(const LogEntryId& _src, Fred::Logger::Api::LogEntryId* _dst)
{
    _dst->set_value(*_src.get_string_id());
}

template <>
void proto_wrap(const LogEntryPropertyValue& _src, Fred::Logger::Api::LogEntryPropertyValue* _dst)
{
    _dst->set_value(*_src.value);
    proto_wrap(_src.children, _dst->mutable_children());
}

template <>
void proto_wrap(const LogEntryPropertyValues& _src, Fred::Logger::Api::LogEntryPropertyValues* _dst)
{
    _dst->mutable_values()->Reserve(_src.size());
    for (const auto& item : _src)
    {
        proto_wrap(item, _dst->add_values());
    }
}

template <>
void proto_wrap(const ObjectReferenceValues& _src, Fred::Logger::Api::ObjectReferenceValues* _dst)
{
    for (const auto& item : _src)
    {
        proto_wrap(item, _dst->add_references());
    }
}

template <>
void proto_wrap(const CreateLogEntryRequest& _src, Fred::Logger::Api::CreateLogEntryRequest* _dst)
{
    proto_wrap(_src.get_service(), _dst->mutable_service());
    proto_wrap(_src.get_type(), _dst->mutable_log_entry_type());

    if (_src.has_source_ip())
    {
        proto_wrap(_src.get_source_ip(), _dst->mutable_source_ip());
    }
    if (_src.has_content())
    {
        proto_wrap(_src.get_content(), _dst->mutable_content());
    }
    if (_src.has_session_id())
    {
        proto_wrap(_src.get_session_id(), _dst->mutable_session_id());
    }
    if (_src.has_properties())
    {
        proto_wrap(_src.get_properties(), _dst->mutable_properties());
    }
    if (_src.has_references())
    {
        proto_wrap(_src.get_references(), _dst->mutable_references());
    }
}

template <>
void proto_wrap(const CloseLogEntryRequest& _src, Fred::Logger::Api::CloseLogEntryRequest* _dst)
{
    proto_wrap(_src.get_log_entry_id(), _dst->mutable_log_entry_id());
    //
    const auto operation_result = _src.get_operation_result();
    if (operation_result.has_code())
    {
        _dst->set_result_code(*operation_result.get_code());
    }
    if (operation_result.has_name())
    {
        _dst->set_result_name(*operation_result.get_name());
    }
    if (_src.has_content())
    {
        proto_wrap(_src.get_content(), _dst->mutable_content());
    }
    if (_src.has_session_id())
    {
        proto_wrap(_src.get_session_id(), _dst->mutable_session_id());
    }
    if (_src.has_properties())
    {
        proto_wrap(_src.get_properties(), _dst->mutable_properties());
    }
    if (_src.has_references())
    {
        proto_wrap(_src.get_references(), _dst->mutable_references());
    }
}

} // namespace LibGrill
