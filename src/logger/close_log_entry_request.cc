/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/close_log_entry_request.hh"

#include <utility>

namespace LibGrill {
namespace Logger {

OperationResult::OperationResult(OperationResultCode _code)
    : content_(OperationResult::Content::code), code_(std::move(_code))
{}

OperationResult::OperationResult(OperationResultName _name)
    : content_(OperationResult::Content::name), name_(std::move(_name))
{}

OperationResult::OperationResult(const OperationResult& _other)
    : content_(_other.content_)
{
    switch(content_)
    {
        case OperationResult::Content::code:
            new(&code_) OperationResultCode{_other.code_};
            break;
        case OperationResult::Content::name:
            new(&name_) OperationResultName{_other.name_};
            break;
    }
}

OperationResult::~OperationResult()
{
    switch(content_)
    {
        case OperationResult::Content::code:
            code_.~OperationResultCode();
            break;
        case OperationResult::Content::name:
            name_.~OperationResultName();
            break;
    }
}

bool OperationResult::has_code() const
{
    return content_ == Content::code;
}

bool OperationResult::has_name() const
{
    return content_ == Content::name;
}

const OperationResultCode& OperationResult::get_code() const
{
    if (!has_code())
    {
        throw std::logic_error("operation result does not contain code");
    }
    return code_;
}

const OperationResultName& OperationResult::get_name() const
{
    if (!has_name())
    {
        throw std::logic_error("operation result does not contain name");
    }
    return name_;
}

CloseLogEntryRequest::CloseLogEntryRequest(const LogEntryId& _log_entry_id, const OperationResultCode& _code)
    : LogEntryRequest(),
      log_entry_id_(_log_entry_id),
      operation_result_(_code)
{}

CloseLogEntryRequest::CloseLogEntryRequest(const LogEntryId& _log_entry_id, const OperationResultName& _name)
    : LogEntryRequest(),
      log_entry_id_(_log_entry_id),
      operation_result_(_name)
{}

LogEntryId CloseLogEntryRequest::get_log_entry_id() const
{
    return log_entry_id_;
}

OperationResult CloseLogEntryRequest::get_operation_result() const
{
    return operation_result_;
}

} // namespace LibGrill::Logger
} // namespace LibGrill
