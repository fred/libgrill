/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/close_log_entry.hh"

#include "src/get_stub.hh"
#include "src/logger/proto_wrap.hh"
#include "src/logger/proto_unwrap.hh"
#include "fred_api/logger/service_logger_grpc.pb.h"

namespace LibGrill {
namespace Logger {

void close_log_entry(Connection<Service::Logger>& _connection, const CloseLogEntryRequest& _close_log_entry_request)
{
    grpc::ClientContext context;
    Fred::Logger::Api::CloseLogEntryRequest api_close_log_entry_request;
    Fred::Logger::Api::CloseLogEntryReply api_close_log_entry_reply;

    proto_wrap(_close_log_entry_request, &api_close_log_entry_request);
    auto stub = get_stub(_connection);
    const auto status = stub.close_log_entry(&context, api_close_log_entry_request, &api_close_log_entry_reply);

    if (not status.ok())
    {
        throw CloseLogEntryException(status.error_code(), status.error_message());
    }

    const auto reply = proto_unwrap(api_close_log_entry_reply);
    throw_if_necessary(reply);
}

} // namespace LibGrill::Logger
} // namespace LibGrill
