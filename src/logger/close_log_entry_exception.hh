/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CLOSE_LOG_ENTRY_EXCEPTION__94A1F862D28C813850BA1D48233A42CA
#define __CLOSE_LOG_ENTRY_EXCEPTION__94A1F862D28C813850BA1D48233A42CA

#include "src/throw_if_necessary.hh"

namespace LibGrill {
namespace Logger {

enum class CloseLogEntryReplyException
{
    log_entry_does_not_exist,
    none
};

} // namespace LibGrill::Logger

template <>
void throw_if_necessary(Logger::CloseLogEntryReplyException);

} // namespace LibGrill

#endif
