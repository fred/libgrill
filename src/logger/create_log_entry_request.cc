/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/create_log_entry_request.hh"

namespace LibGrill {
namespace Logger {

CreateLogEntryRequest::CreateLogEntryRequest(
        const LogEntryType& _type,
        const LogEntryService& _service)
    : LogEntryRequest(),
      type_(_type),
      service_(_service)
{
}

void CreateLogEntryRequest::add(const SourceIp& _source_ip)
{
    source_ip_ = _source_ip;
}

bool CreateLogEntryRequest::has_source_ip() const
{
    return static_cast<bool>(source_ip_);
}

LogEntryType CreateLogEntryRequest::get_type() const
{
    return type_;
}

LogEntryService CreateLogEntryRequest::get_service() const
{
    return service_;
}

SourceIp CreateLogEntryRequest::get_source_ip() const
{
    return source_ip_.value();
}

} // namespace LibGrill::Logger
} // namespace LibGrill
