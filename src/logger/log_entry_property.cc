/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libgrill/logger/log_entry_property.hh"

namespace LibGrill {
namespace Logger {

namespace {

LogEntryPropertyValue& insert_or_update(Properties& _dst, const Properties::value_type& _item)
{
    auto it = _dst.find(_item.first);
    if (it == _dst.end())
    {
        return _dst.insert(_item).first->second.back();
    }
    it->second.insert(it->second.end(), _item.second.begin(), _item.second.end());
    return it->second.back();
}

} // namespace LibGrill::Logger::{anonymous}

LogEntryPropertyValue& add(Properties& _dst, const LogEntryProperty::Name& _type, const LogEntryPropertyValue& _value)
{
    return insert_or_update(_dst, std::make_pair(_type, std::vector<LogEntryPropertyValue>{_value}));
}

LogEntryPropertyValue& add(Properties& _dst, const LogEntryProperty& _value)
{
    return insert_or_update(_dst, std::make_pair(_value.name, std::vector<LogEntryPropertyValue>{LogEntryPropertyValue{_value.value,{}}}));
}

void merge(Properties& _dst, Properties&& _src)
{
    for (const auto& item : _src)
    {
        insert_or_update(_dst, item);
    }
}

} // namespace LibGrill::Logger
} // namespace LibGrill
