/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/object_reference.hh"

namespace LibGrill {
namespace Logger {

void insert_or_update(ObjectReferences& _dst, const ObjectReferences::value_type& _item)
{
    auto it = _dst.find(_item.first);
    if (it == _dst.end())
    {
        _dst.insert(_item).first;
    }
    else
    {
        it->second.insert(it->second.end(), _item.second.begin(), _item.second.end());
    }
}

void add(ObjectReferences& _dst, const ObjectReference& _src)
{
    insert_or_update(_dst, std::make_pair(_src.type, _src.values));
}

void merge(ObjectReferences& _dst, ObjectReferences&& _src)
{
    for (const auto& item : _src)
    {
        insert_or_update(_dst, item);
    }
}

} // namespace LibGrill::Logger
} // namespace LibGrill
