/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/logger/log_entry_id.hh"

#include <algorithm>

namespace LibGrill {
namespace Logger {

namespace {

std::vector<std::string> split_on(const std::string& _in_string, const char _delimiter)
{
    std::vector<std::string> out_tokens;
    auto beg_ptr = _in_string.begin();
    auto end_ptr = _in_string.end();
    auto nxt_ptr = std::find(beg_ptr, end_ptr, _delimiter);
    while (nxt_ptr != end_ptr)
    {
        out_tokens.emplace_back(std::string(beg_ptr, nxt_ptr));
        beg_ptr = nxt_ptr + 1;
        nxt_ptr = std::find(beg_ptr, end_ptr, _delimiter);
    }
    out_tokens.emplace_back(beg_ptr, nxt_ptr);
    return out_tokens;
}

std::vector<std::string> split_log_entry_string_id(const LogEntryId::StringId& _string_id)
{
    static const char delimiter = '.';
    auto out_tokens = split_on(*_string_id, delimiter);
    if (out_tokens.size() != 4)
    {
        throw std::invalid_argument("incorrect number of tokens in log entry id");
    }
    return out_tokens;
}

bool parse_monitoring_status(const std::string& _src)
{
    if (_src == "0")
    {
        return false;
    }
    else if (_src == "1")
    {
        return true;
    }
    throw std::invalid_argument("monitoring status should be 0 or 1");
}

LogEntryId::NumericalId parse_numerical_id(const std::string& _src)
{
    try
    {
        return LogEntryId::NumericalId{std::stoull(_src)};
    }
    catch (const std::exception& e)
    {
        throw std::invalid_argument(e.what());
    }
}

LogEntryId::TimeBegin parse_time_begin(const std::string& _src)
{
    return LogEntryId::TimeBegin{_src};
}

LogEntryService parse_service(const std::string& _src)
{
    return LogEntryService{_src};
}

} // namespace LibGrill::Logger

LogEntryId::LogEntryId(const LogEntryId::StringId& _value)
    : LogEntryId(_value, split_log_entry_string_id(_value))
{
}

LogEntryId::LogEntryId(const LogEntryId::StringId& _value, const std::vector<std::string>& _tokens)
    : unparsed_value_(_value),
      numerical_id_(parse_numerical_id(_tokens[0])),
      time_begin_(parse_time_begin(_tokens[1])),
      service_(parse_service(_tokens[2])),
      monitoring_status_(parse_monitoring_status(_tokens[3]))
{
}

LogEntryId::NumericalId LogEntryId::get_numerical_id() const
{
    return numerical_id_;
}

LogEntryId::StringId LogEntryId::get_string_id() const
{
    return unparsed_value_;
}

LogEntryId::operator LogEntryId::NumericalId() const
{
    return numerical_id_;
}

LogEntryId::operator LogEntryId::StringId() const
{
    return unparsed_value_;
}

LogEntryService LogEntryId::get_service() const
{
    return service_;
}

LogEntryId::TimeBegin LogEntryId::get_time_begin() const
{
    return time_begin_;
}

bool LogEntryId::is_from_monitored_ip_address() const
{
    return monitoring_status_;
}

} // namespace LibGrill::Logger
} // namespace LibGrill
