/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __PROTO_UNWRAP__7B7ADF93E1B5425CCF7EB9839AB662E7
#define __PROTO_UNWRAP__7B7ADF93E1B5425CCF7EB9839AB662E7

namespace LibGrill {

template <typename>
struct ProtoTypeTraits
{
    struct InternalType;
};

template <typename ProtoType>
typename ProtoTypeTraits<ProtoType>::InternalType proto_unwrap(const ProtoType&);

} // namespace LibGrill

#endif
