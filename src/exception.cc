/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "include/libgrill/exception.hh"

namespace LibGrill {

Exception::Exception(std::string _msg)
    : msg_{std::move(_msg)}
{
}

const char* Exception::what() const noexcept
{
    return msg_.c_str();
}

GrpcException::GrpcException(int _grpc_error_status_code, std::string _grpc_status_error_message)
    : Exception("gRPC: error code: " + std::to_string(_grpc_error_status_code) + ", error message: " + _grpc_status_error_message),
      grpc_status_error_code_(_grpc_error_status_code),
      grpc_status_error_message_(std::move(_grpc_status_error_message))
{
}

int GrpcException::error_code() const
{
    return grpc_status_error_code_;
}

std::string GrpcException::error_message() const
{
    return grpc_status_error_message_;
}

} // namespace LibGrill
