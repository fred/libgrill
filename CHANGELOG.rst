CHANGELOG
=========

.. contents:: Releases
   :backlinks: none
   :local:

3.0.0 (2024-09-18)
------------------

* Switch to API 4.2.0

2.0.0 (2023-03-08)
------------------

* Changed log entry id delimiter
* Important fixes of properties and references

1.0.1 (2021-09-16)
------------------

* Fix distcheck (cmake)


1.0.0 (2021-09-10)
------------------

* Initial release
